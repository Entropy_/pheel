#!/bin/bash
#This script parses two folders deep and sed's them, this particualar sedup
#will not throw any errors if this script is placed in the folder above and run
#from there.

appreplace="%%APPNAME%%"
hostreplace="%%HOSTNAME%%"
userreplace="%%USERNAME%%"

echo -e "\033[38;2;0;255;0mChanging user directory.\033[0m"
#sed -i 's/'$userreplace'/'$USER'/g' pheel/pheep.go
for e in $( ls pheel/* ); do
  [ ${e: -1} == ":" ] && break
  echo -e "\033[38;2;0;200;150mFound "$e
  echo -e "\033[38;2;0;255;0mSetting " $e "\033[0m"
  sed -i s/$userreplace/$USER/g $e
done
for d in $( ls pheel/phel/* ); do
  [ ${d: -1} == ":" ] && continue
  echo -e "\033[38;2;0;200;150mFound "$d
  echo -e "\033[38;2;0;255;0mSetting " $d "\033[0m"
  sed -i s/$userreplace/$USER/g $d
done

echo -e "\033[48;2;0;255;0mDone\033[0m"
echo -e "\033[38;2;150;150;0mPlease enter your app name in the following format. (\033[38;2;250;100;0mAppName\033[38;2;150;150;0m)\033[0m\033[0m"
read AppName
#sed -i 's/%%APPNAME%%/'$A'/g' libs/application.go
#echo "Setting app name to "$AppName"."
#ls libs | grep *.go | sed -i 's/%%APPNAME%%/'$A'/g' pheep.go
for i in $( ls pheel/* ); do
  [ ${i: -1} == ":" ] && break
  echo -e "\033[38;2;0;200;150mFound "$i
  echo -e "\033[38;2;0;255;0mSetting " $i "\033[0m"
  sed -i s/$appreplace/$AppName/g $i
done

for a in $( ls pheel/phel/* ); do
  [ ${a: -1} == ":" ] && continue
  echo -e "\033[38;2;0;200;150mFound " $a
  echo -e "\033[38;2;0;255;0mSetting " $i "\033[0m"
  sed -i s/$appreplace/$AppName/g $a
done
#ls pheel | grep -r *".go" pheel/. | sed -i --expression 's/'$appreplace'/'$AppName'/g'

echo -e "\033[38;2;150;150;0mPlease enter your valid hostname in the following format. (\033[38;2;250;100;0msquirrel.pizza\033[38;2;150;150;0m)\033[0m"
read vHostname
echo -e "\033[38;2;150;150;0mGetting a list of hostnames that need to be changed.\033[0m"
#ls tests/ | grep -r "snowcrash.network" tests/ | sed -i 's/snowcrash.network/'$B'/g'
echo -e "\033[38;2;150;150;0mChanging hostnames to \033[38;2;0;150;0m"$vHostname".\033[0m"
for b in $( ls pheel/* ); do
  [ ${b: -1} == ":" ] && break
  echo -e "\033[38;2;0;200;150mFound "$b
  echo -e "\033[38;2;0;255;0mSetting " $b "\033[0m"
  sed -i s/$hostreplace/$vHostname/g $b
done

for c in $( ls pheel/phel/* ); do
  [ ${c: -1} == ":" ] && continue
  echo -e "\033[38;2;0;200;150mFound " $c
  echo -e "\033[38;2;0;255;0mSetting " $c "\033[0m"
  sed -i s/$hostreplace/$vHostname/g $c
done
#ls pheel | grep -r *".go" pheel/. | sed -i --expression 's/'$hostreplace'/'$vHostname'/g'
echo -e "\033[48;2;0;255;0mDone\033[0m"
